﻿Feature: Calculator Scientific Operations
	
	Background: 
Given I enter View > Scientific in the menu


Scenario: Factorial
	When I set the number 9 on the calculator
	And I press ! on the calculator
	And I press = on the calculator
	Then Result should be 362880 on the calculator

Scenario: y sqrt x
	When I set the number 9 on the calculator
	And I press y√x on the calculator
	And I set the number 2 on the calculator
	And I press = on the calculator
	Then Result should be 3 on the calculator

Scenario: 3 sqrt x
	When I set the number 27 on the calculator
	And I press 3√x on the calculator
	And I press = on the calculator
	Then Result should be 3 on the calculator

Scenario: 10 pow x
	When I set the number 5 on the calculator
	And I press 10^x on the calculator
	And I press = on the calculator
	Then Result should be 100000 on the calculator

Scenario: x pow 2
	When I set the number 5 on the calculator
	And I press x^2 on the calculator
	And I press = on the calculator
	Then Result should be 25 on the calculator

Scenario: x pow y
	When I set the number 2.5 on the calculator
	And I press x^y on the calculator
	And I set the number 3 on the calculator
	And I press = on the calculator
	Then Result should be 15.625 on the calculator

Scenario: x pow 3
	When I set the number 4.0 on the calculator
	When I press ± on the calculator
	And I press x^3 on the calculator
	And I press = on the calculator
	Then Result should be -64 on the calculator

Scenario: log
	When I set the number 100 on the calculator
	And I press log on the calculator
	And I press = on the calculator
	Then Result should be 2 on the calculator

Scenario: ln (natural log)
	When I set the number 10 on the calculator
	And I press ln on the calculator
	And I press = on the calculator
	Then Result should be 2.3025850929940456840179914546844 on the calculator

Scenario: sin
	When I set the number 90 on the calculator
	And I press sin on the calculator
	And I press = on the calculator
	Then Result should be 1 on the calculator
	
Scenario: cos
	When I set the number 90.0 on the calculator
	And I press cos on the calculator
	And I press = on the calculator
	Then Result should be 0 on the calculator
		
Scenario: tan
	When I set the number 45 on the calculator
	And I press tan on the calculator
	And I press = on the calculator
	Then Result should be 1 on the calculator
		
Scenario: Modulo (rest of the division)
	When I set the number 5 on the calculator
	And I press Mod on the calculator
	And I set the number 3 on the calculator
	And I press = on the calculator
	Then Result should be 2 on the calculator
			
Scenario: sinh (Hyperbolic sine)
	When I set the number 1 on the calculator
	And I press sinh on the calculator
	And I press = on the calculator
	Then Result should be 1.1752011936438014568823818505956 on the calculator
				
Scenario: cosh (Hyperbolic cosine)
	When I set the number 1 on the calculator
	And I press cosh on the calculator
	And I press = on the calculator
	Then Result should be 1.5430806348152437784779056207571 on the calculator
					
Scenario: tanh (Hyperbolic tangent)
	When I set the number 1 on the calculator
	And I press tanh on the calculator
	And I press = on the calculator
	Then Result should be 0.76159415595576488811945828260479 on the calculator
						
Scenario: Exponential view
	When I set the number 100 on the calculator
	And I press Exp on the calculator
	Then Result should be 100.e+0 on the calculator
							
Scenario: Integer part
	When I set the number 10.155 on the calculator
	And I press ± on the calculator
	And I press Int on the calculator
	Then Result should be -10 on the calculator
	
Scenario: Degree minute second (Operations for angular quantities)
	When I set the number 10.1 on the calculator
	And I press dms on the calculator
	And I press = on the calculator
	Then Result should be 10.06 on the calculator
		
Scenario: Pi
	When I press π on the calculator
	Then Result should be 3.1415926535897932384626433832795 on the calculator
	
Scenario: Exponential notation
	When I set the number 5896 on the calculator
	And I press F-E on the calculator
	Then Result should be 5.896e+3 on the calculator
		
Scenario: sin−1 (Inverse function to sine)
	When I press Inv on the calculator
	And I set the number 1 on the calculator
	And I press sin−1 on the calculator
	Then Result should be 90 on the calculator
	 
	@RadioButton
Scenario: Switching to "Grads" units, setting the value and calculating sin of this value
	When I switch Grads on the calculator
	And I set the number 100 on the calculator
	And I press sin on the calculator
	Then Result should be 1 on the calculator

	@RadioButton
Scenario: Switching to "Radians" units, setting the value and calculating tan of this value
	When I switch Radians on the calculator
	And I set the number 0.78539816339745 on the calculator
	And I press tan on the calculator
	Then Result should be 1.000000000000003380768678308366 on the calculator

	@CheckHistory
Scenario: Make sure "History" stores all steps of preview calculations
	When I set the number 12 on the calculator
	And I press - on the calculator
	And I set the number 2 on the calculator
	And I press = on the calculator
	And I press + on the calculator
	And I set the number 3 on the calculator
	And I press = on the calculator
	Then Result on history screen should be 12 - 2 10 + 3 
		