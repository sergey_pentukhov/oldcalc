﻿Feature: Calculator Simple Operations
	In order to avoid silly mistakes
	As a math idiot
	I want to check operations with numbers

Scenario: Add
	When I set the number 4.589 on the calculator
	And I press + on the calculator
	And I set the number 10 on the calculator
	And I press = on the calculator
	Then Result should be 14.589 on the calculator

Scenario: Add many
	When I set the number 1.1 on the calculator
	And I press + on the calculator
	When I set the number 2.2 on the calculator
	And I press + on the calculator
	When I set the number 3.3 on the calculator
	And I press + on the calculator
	When I set the number 4.4 on the calculator
	And I press = on the calculator
	Then Result should be 11 on the calculator

Scenario: Multiply
	When I set the number 1 on the calculator
	And I press * on the calculator
	And I set the number 3 on the calculator
	And I press = on the calculator
	Then Result should be 3 on the calculator

Scenario: Subtract
	When I set the number 4454.5 on the calculator
	And I press - on the calculator
	And I set the number 10 on the calculator
	And I press = on the calculator
	Then Result should be 4444.5 on the calculator

Scenario: Divide
	When I set the number 9 on the calculator
	And I press / on the calculator
	And I set the number 6 on the calculator
	And I press = on the calculator
	Then Result should be 1.5 on the calculator

	@UnitConversion
Scenario: Check convert Lenght	
	Given I enter View > Unit conversion in the menu
	When I choose Length into Select the type of unit you want to convert list
	And I choose Mile into From list
	And I type 1 into the From field
	And I choose Meter into To list
	Then Result into To field should be 1609.344 on the calculator

	@Worksheets
Scenario: Mortgage
	Given I enter View > Worksheets > Mortgage  in the menu
	When I choose Monthly payment into Select the value you want to calculate list
	And I type 120 into the Purchase price field
	And I type 0 into the Down payment field
	And I type 1 into the Term (years) field
	And I type 10 into the Interest rate (%) field
	And I press Calculate on the calculator
	Then Result into Monthly payment field should be 10.54990646760115 on the calculator

		     


