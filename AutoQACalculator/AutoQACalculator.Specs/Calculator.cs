﻿
namespace AutoQACalculator.Specs
{
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Threading;

    using AutoQACalculator.Specs.Utils;

    using TestStack.White;
    using TestStack.White.UIItems;
    using TestStack.White.UIItems.Finders;
    using TestStack.White.UIItems.ListBoxItems;
    using TestStack.White.UIItems.WindowItems;

    public class Calculator
    {
        private Window Window => this.CalculatorApplication.GetWindow(CalculatorConstants.CalculatorWindowName);

        private Application CalculatorApplication { get; set; }

        public void CreateCalculator()
        {
            if (!File.Exists(CalculatorConstants.PathToCalculator))
            {
                Logger.Debug("Application is installing..");

                var processStartInfo = new ProcessStartInfo(CalculatorConstants.ExecutedFileName, CalculatorConstants.CmdArgs); 
                Process.Start(processStartInfo);
                Thread.Sleep(2000);
                Logger.Info("Application was installed..");
            }
                
            this.CalculatorApplication = Application.Launch(CalculatorConstants.PathToCalculator);
            Logger.Info("===Open Calculator===");
        }

        public void CloseCalculator()
        {
            this.CalculatorApplication.Close();
            Logger.Info("===Close Calculator===\n\r\n\r");
        }

        public void PushButton(string buttonNameOrSign)
        {
            var buttonName = CalculatorConstants.OperationsDictionary.ContainsKey(buttonNameOrSign) ?
                                 CalculatorConstants.OperationsDictionary[buttonNameOrSign]
                                 : buttonNameOrSign;

            this.Window.Get<Button>(SearchCriteria.ByText(buttonName)).Click();
            Logger.Info("Button is clicked : " + buttonName);
        }

        public string ResultOfOperations()
        {
            var text = this.Window.Get<Label>(SearchCriteria.ByAutomationId(CalculatorConstants.IdResultTextBox)).Text;
            Logger.Info("The result on the screen was got: " + text);
            return text;
        }

        public string ResultOfTextBox(string textBoxName)
        {
            var text = this.Window.Get<TextBox>(textBoxName).Text;
            Logger.Info("The result on the textBox was got: " + text);
            return text;
        }

        public string ResultOfHistoryTextBox()
        {
            var listItems = this.Window.Get<ListBox>(SearchCriteria.ByAutomationId(CalculatorConstants.IdHistoryListTextBox)).Items;
            var stringAppenderOfAllSteps = new StringBuilder();
            listItems.ForEach(item => stringAppenderOfAllSteps.Append(item.Name + " "));
            Logger.Info("The result on the History screen in one row: " + listItems);
            return stringAppenderOfAllSteps.ToString();
        }

        public void ClickMenuItem(params string[] menuPath)
        {
            var menuItem = this.Window.MenuBar.MenuItem(menuPath);
            menuItem.Click();
            Logger.Info("Clicked on menu items!");
        }

        public void SwitchRadioButton(string radioButtonName)
        {
            Logger.Debug("Search radioButton by text: " + radioButtonName);
            var radioButton = this.Window.Get<RadioButton>(SearchCriteria.ByText(radioButtonName));
            radioButton.Click();
            Logger.Info("RadioButton was switched: " + radioButtonName);
        }

        public void ChooseListItem(string listItemName, string comboBoxName)
        {
            Logger.Debug("Choosing on comboBox \"" + listItemName + "\" listItem: " + listItemName);
            var comboBox = this.Window.Get<ComboBox>(comboBoxName);
            comboBox.Select(listItemName);
            Logger.Info("Chose!");
        }

        public TextBox FindField(string fieldName)
        {
            Logger.Debug("Searching textBox .. " + fieldName);
            var textBox = this.Window.Get<TextBox>(fieldName);
            Logger.Info("TextBox was founded!");
            return textBox;
        }
    }
}

