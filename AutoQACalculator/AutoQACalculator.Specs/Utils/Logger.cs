﻿
namespace AutoQACalculator.Specs.Utils
{
    using log4net;
    using log4net.Config;

    public static class Logger
    {
        static Logger()
        {
            XmlConfigurator.Configure();
        }

        public static ILog Log => LogManager.GetLogger("LOGGER");

        public static void Info(string message)
        {
            Log.Info(message);
        }

        public static void Debug(string message)
        {
            Log.Debug(message);
        }

        public static void Error(string message)
        {
            Log.Error(message);
        }
    }
}