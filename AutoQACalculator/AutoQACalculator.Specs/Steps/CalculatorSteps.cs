﻿
namespace AutoQACalculator.Specs.Steps
{
    using AutoQACalculator.Specs.Utils;

    using NUnit.Framework;
    using TechTalk.SpecFlow;

    [Binding]
    public sealed class CalculatorSteps
    {
        private readonly Calculator calculator = new Calculator();

        [BeforeScenario]
        public void OpenCalculator()
        {
            this.calculator.CreateCalculator();
            Logger.Info("+++ SCENARIO: " + ScenarioContext.Current.ScenarioInfo.Title + " +++");
        }

        [BeforeScenario("CheckHistory")]
        [AfterScenario("CheckHistory")]
        public void OnOffHistoryFunction()
        {
            this.EnterMenuItem(CalculatorConstants.PathToHistory);
        }

        [Given(@"I enter (.*) in the menu")]
        public void EnterMenuItem(string menuPath)
        {
            Logger.Debug("Clicking on menu item: " + menuPath);
            var menuPathArray = menuPath.Split(CalculatorConstants.MenuPathSeparator);
            for (int i = 0; i < menuPathArray.Length; i++)
            {
                menuPathArray[i] = menuPathArray[i].Trim();
            }
            this.calculator.ClickMenuItem(menuPathArray);
        }

        [When(@"I choose (.*) into (.*) list")]
        public void ChooseListItem(string listItemName, string listName)
        {
            this.calculator.ChooseListItem(listItemName, listName);
        }
       
        [When(@"I type (-?\d+\.?\d*?) into the (.*) field")]
        public void TypeNumberInField(string number, string fieldName)
        {
            var field = this.calculator.FindField(fieldName);
            field.Focus();
            this.SetNumber(number);
        }

        [When(@"I set the number (\d+\.?\d*?) on the calculator")]
        public void SetNumber(string number)
        {
            Logger.Info("Is a number set.. " + number);
            var charArray = number.ToCharArray();
            foreach (var ch in charArray)
            {
                if (ch.Equals(CalculatorConstants.Separator))
                {
                    var buttonName = CalculatorConstants.SeparatorButtonName;
                    this.calculator.PushButton(buttonName);
                }
                else
                {
                    this.calculator.PushButton(ch.ToString());
                }
            }

            Logger.Info("The setting a number is finished.. ");
        }

        [When(@"I press ([/*!+-=±]|10\^x|3√x|y√x|x\^2|x\^3|log|x\^y|ln|sin|cos|tan|Mod|
                                            |Calculate|sinh|cosh|tanh|Exp|Int|dms|π|F-E|Inv|sin−1) on the calculator")]
        public void PressButton(string buttonName)
        {
            this.calculator.PushButton(buttonName);
        }

        [When(@"I switch (Grads|Degrees|Radians) on the calculator")]
        public void SwitchUnits(string radioButtonName)
        {
            this.calculator.SwitchRadioButton(radioButtonName);
        }

        [Then(@"Result should be (-?\d+\.?[e+\d]*?\d*?) on the calculator")]
        public void GetResult(string number)
        {
            var doubleExpexted = double.Parse(number);
            var doubleActual = double.Parse(this.calculator.ResultOfOperations());

            Assert.AreEqual(doubleExpexted, doubleActual, "expectResult - " + doubleExpexted + " , but was - " + doubleActual);
        }

        [Then(@"Result on history screen should be (.*)")]
        public void GetHistoryResult(string expectedResult)
        {
            var actualResult = this.calculator.ResultOfHistoryTextBox().TrimEnd(' ');
            Assert.AreEqual(expectedResult, actualResult, "expectResult - " + expectedResult + " , but was - " + actualResult);
        }

        [Then(@"Result into (.*) field should be (-?\d+\.?\d*?) on the calculator")]
        public void GetTextBoxResult(string fieldName, string expectedResult)
        {
            var actualResult = this.calculator.ResultOfTextBox(fieldName);
            Assert.AreEqual(expectedResult, actualResult, "expectResult - " + expectedResult + " , but was - " + actualResult);
        }

        [AfterScenario]
        public void CloseCalculator()
        {
            if (ScenarioContext.Current.TestError != null)
            {
                Logger.Error("TEST FAILED!\n\r" + ScenarioContext.Current.TestError.Message);
            }

            this.calculator.CloseCalculator();
        }
    }
}
