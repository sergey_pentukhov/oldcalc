﻿
namespace AutoQACalculator.Specs
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public static class CalculatorConstants
    {
        public const string IdResultTextBox = "150";

        public const string IdHistoryListTextBox = "153";
            
        public const string CalculatorWindowName = "Calculator";

        public const string ExecutedFileName = "cmd.exe";

        public const string ApplicationName = "calc1.exe";

        public const string RunInstallFileName = "RunInstallation.bat";

        public const char Separator = '.';

        public const char MenuPathSeparator = '>';

        public const string SeparatorButtonName = "Decimal separator";

        public const string PathToHistory = "View > History";

        public static string CmdArgs => "/C " + Path.Combine(
                                            AppDomain.CurrentDomain.BaseDirectory,
                                            "..\\..\\..\\..\\",
                                            RunInstallFileName);

        public static string PathToCalculator => Path.Combine(Environment.SystemDirectory, ApplicationName);

        public static Dictionary<string, string> OperationsDictionary =>
                new Dictionary<string, string>
                    {
                        { "+", "Add" },
                        { "-", "Subtract" },
                        { "/", "Divide" },
                        { "*", "Multiply" },
                        { "=", "Equals" },
                        { "±", "Negate" },
                        { "y√x", "Order of y root" },
                        { "3√x", "Cube root" },
                        { "!", "Factorial" },
                        { "10^x", "Anti log" },
                        { "log", "Log" },
                        { "x^2", "Square" },
                        { "x^3", "Cube" },
                        { "x^y", "Raise to y power" },
                        { "ln", "Natural log" },
                        { "sin", "Sine" },
                        { "cos", "Cosine" },
                        { "tan", "Tangent" },
                        { "Mod", "Modulo" },
                        { "sinh", "Hyperbolic sine" },
                        { "cosh", "Hyperbolic cosine" },
                        { "tanh", "Hyperbolic tangent" },
                        { "Exp", "Exponential" },
                        { "Int", "Integer part" },
                        { "dms", "Degree minute second" },
                        { "π", "Pi" },
                        { "F-E", "Exponential notation" },
                        { "Inv", "Inverse functions" },
                        { "sin−1", "Sine inverse" },
                        { "Grads", "Grads" },
                        { "Radians", "Radians" },
                        { "Degrees", "Degrees" }
                    };
        }
}